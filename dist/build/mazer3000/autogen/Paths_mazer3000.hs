{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_mazer3000 (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/william/.cabal/bin"
libdir     = "/home/william/.cabal/lib/x86_64-linux-ghc-8.6.5/mazer3000-0.1.0.0-7baVrSsw27aAtBhao8uCK-mazer3000"
dynlibdir  = "/home/william/.cabal/lib/x86_64-linux-ghc-8.6.5"
datadir    = "/home/william/.cabal/share/x86_64-linux-ghc-8.6.5/mazer3000-0.1.0.0"
libexecdir = "/home/william/.cabal/libexec/x86_64-linux-ghc-8.6.5/mazer3000-0.1.0.0"
sysconfdir = "/home/william/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "mazer3000_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "mazer3000_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "mazer3000_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "mazer3000_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "mazer3000_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "mazer3000_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
