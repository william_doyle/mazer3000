// js rule 110 golf

a = "1"
b = "0"
function c(x) {
  switch (x) {
    case "111": return b
    case "110": return a
    case "101": return a
    case "100": return b
    case "011": return a
    case "010": return a
    case "001": return a
    case "000": return b
  }
}

function d(x) {
  //let y = showIntAtBase 2 intToDigit x""
  let y = x.toString(2)
  if (y.length >= 8)
    return y
  else
    return `${h("", (8 - y.length))}${y}`
}
const e = (x, y) => {
  if (x.length == 3)
    return x
  if (y == null)
    return `0${x}`
  return `${x} ${h("", (3 - x.length))}`
}

const f = (x, y) => {
  let z = h("", ((y - (x.length / 2)) - 2))
  return `${z}${x}${z}`
}

g = (x, y, w, u, v, z) => {
  if (w >= u)
    return v
  else {
    let t = i(x, y, "", u, z)
    return g(t, y(w + 1), u, (`${v}\n${t}`), z)
  }
}


h = (x, y) => {
  if (y < 1) return ""
  if (x.length == y) return x
  return h(`${x}0`, y)
}