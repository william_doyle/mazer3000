module Resistor (
         Resistor,       -- abstract, hiding constructors
         metalResistor,  -- only way to build a metal resistor
       ) where

data Resistor = Metal   Bands
              | Ceramic Bands 
                deriving Show

type Bands = Int

metalResistor :: Bands -> Resistor
metalResistor n | n < 4 || n > 8 = error "Invalid number of resistor bands" 
                | otherwise      = Metal n


