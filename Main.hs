{-# LANGUAGE LambdaCase #-}
module Main where
import Data.List
import Resistor
type ChamberRow = [String]
type Chamber = [ChamberRow]
type ChamberPosition = (Int, Int)

-- OUTER WALLS
topWall :: Int -> ChamberRow
topWall size = [ "1", "0"] ++ (take (size - 2) (repeat "1") )

bottomWall :: Int -> ChamberRow
bottomWall size = (take (size - 2) (repeat "1") ) ++ [ "0", "1" ] 

emptyMazeBodyPiece :: Int -> ChamberRow
emptyMazeBodyPiece size = [ "1" ] ++ (take (size - 2) (repeat "0") ) ++ ["1"]

-- COMBINE OUTER WALLS TO MAKE EMPTY MAZE 
emptyMaze :: Int -> Chamber
emptyMaze size = [topWall size] ++  take (size - 2) (repeat (emptyMazeBodyPiece size)) ++ [ bottomWall size ]

-- DISPLAY THE MAZE
rowToString :: ChamberRow -> String
rowToString row = foldl (\acc curr -> acc ++ " " ++ curr) "" row 

toText :: Chamber -> String
toText mBody = foldl (\acc curr -> acc ++ "\n" ++ (rowToString curr)) "" mBody 

replace_ a b s = concatMap(\case x | x == a    -> b 
                                   | otherwise -> [x]) s

style :: String -> String
style maze = replace_ '0' " " (replace_ '1' "■" maze)

showMaze :: Chamber -> IO()
showMaze maze = putStrLn (style (toText maze))

-- ADD WALL
findOrientation :: ChamberPosition -> ChamberPosition -> String
findOrientation a b 
  | fst a == fst b = "hoz"
  | snd a == snd b = "vert"
  | otherwise      = "Nothing"

findOrientation' :: ChamberPosition -> ChamberPosition -> Maybe String
findOrientation' a b 
  | fst a == fst b = Just "hoz"
  | snd a == snd b = Just "vert"
  | otherwise      = Nothing

findCommonAxisValue :: ChamberPosition -> ChamberPosition -> Int
findCommonAxisValue a b 
  | findOrientation' a b == Just "hoz" = fst a
  | otherwise = snd a 

rotate :: Chamber -> Chamber
rotate chamber = do
  let n = 1 :: Int
  (iterate(transpose.reverse) chamber) !! n

rotateNTimes :: Int -> Chamber -> Chamber
rotateNTimes n chamber 
  | n == 0    = chamber
  | otherwise = rotateNTimes (n - 1) (rotate chamber)

rotate3 :: Chamber -> Chamber
rotate3 chamber = rotateNTimes 3 chamber

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
sizeBasedSelection :: (Int -> Int -> Bool) -> Int -> Int -> Int 
sizeBasedSelection function a b
  | (function) a b = a
  | otherwise      = b
 
largerOf :: Int -> Int -> Int
largerOf = sizeBasedSelection (>)

smallerOf :: Int -> Int -> Int
smallerOf = sizeBasedSelection (<)
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

inRange :: Int -> Int -> Int -> Bool
inRange min max val 
  | (val >= min && val <= max) = True 
  | otherwise =  False

uncurry f = (\x y -> f(x, y))
mapi f arr = zipWith (Main.uncurry f) [0..] arr -- map with index and element (together as a tuple) 

calculateUncommonAxisValues :: ChamberPosition -> ChamberPosition -> String -> (Int, Int) 
calculateUncommonAxisValues a b ori
  | (ori == "hoz") = (snd a, snd b) 
  | otherwise      = (fst a, fst b) 


addWall :: Chamber -> ChamberPosition -> ChamberPosition -> Chamber
addWall chamber startPosition endPosition = do
  let orientation = findOrientation startPosition endPosition
  let commonAxisValue = findCommonAxisValue startPosition endPosition
  let uncommonAxisValues = calculateUncommonAxisValues startPosition endPosition orientation
  let wallLength = abs (fst uncommonAxisValues - snd uncommonAxisValues)
  let wallStart = smallerOf (fst uncommonAxisValues) (snd uncommonAxisValues) 
  let wallEnd = largerOf (fst uncommonAxisValues) (snd uncommonAxisValues) 
  
  -- rotate (if vert)
  let maybeRotated = (\case (m, ori) | ori == "vert" -> rotate m
                                     | otherwise     -> m ) (chamber, orientation)
  -- add the wall
  let inWallRange = inRange wallStart wallEnd
  let withNewWall = mapi (\case (rowIndex, row) | rowIndex /= toInteger commonAxisValue -> row 
                                                | otherwise                             -> mapi (\case (elIndex, el) | inWallRange elIndex -> "1"
                                                                                                                     | otherwise           -> el ) row ) maybeRotated

  -- rotate again (if vert)
  (\case (m, ori) | ori == "vert" -> rotate3 m 
                  | otherwise     -> m ) (withNewWall, orientation)

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
halfChamber :: (Int -> Chamber -> Chamber) -> (Int -> ChamberRow -> ChamberRow) -> Chamber -> Chamber
halfChamber sideSelectFunctionA sideSelectFunctionB chamber = do 
  let size = length chamber :: Int
  let half = size `div` 2 :: Int
  sideSelectFunctionA half (map (\row -> sideSelectFunctionB half row) chamber ) 

topLeftChamber :: Chamber -> Chamber
topLeftChamber chamber = halfChamber take take chamber

topRightChamber :: Chamber -> Chamber
topRightChamber chamber = halfChamber take drop chamber

bottomLeftChamber :: Chamber -> Chamber
bottomLeftChamber chamber = halfChamber drop take chamber

bottomRightChamber :: Chamber -> Chamber
bottomRightChamber chamber = halfChamber drop drop chamber
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

process :: Chamber -> Chamber
process chamber = do
  let size = length chamber :: Int
  let half = size `div` 2 :: Int

  if (size <= 4) then chamber
  else do 
    let topLeft       = topLeftChamber chamber

    let topRight      = topRightChamber chamber 
    let _topRight     = addWall topRight (1, 0) ( size , 0)
    
    let top           = zipWith (++) (process topLeft)(process _topRight)
    let _top          = addWall top (half -1, 1) (half - 1, size)--(init top) ++ [(take size (repeat "1") )]

    let bottomLeft    = bottomLeftChamber chamber
    
    let bottomRight   = drop half ( map (\row -> drop half row) chamber)
    let _bottomRight  = addWall bottomRight (0, 0) (size, 0)

    let bottom        = zipWith (++) (process bottomLeft)(process _bottomRight)
    let whole         = _top ++ bottom
    whole

main :: IO ()
main = do 
  let mazeSize = 19
  
  let body = emptyMaze mazeSize
  
  showMaze body
  let part = process body 
  showMaze part

  let demo = metalResistor 4  
  print demo

  {-
  
  showMaze (addWall body (2, 2) (2, 39))
  showMaze (addWall body (2, 2) (39, 2))
  putStrLn "about to demo rotations"
  showMaze (rotate part)
  showMaze (rotateNTimes 2 part)
  showMaze (rotate3 part)
  putStrLn (findOrientation (2, 4) (2, 5)) 
  putStrLn (findOrientation (3, 4) (2, 4)) 
  putStrLn (findOrientation (0, 4) (2, 5)) 


  print (smallerOf 1 2)
  print (largerOf 1 2)
-}

-- next time: make sure walls are never doubled up when guard is 4 instead of 6
-- do this by making a simple way to add a wall... finish the addWall function
